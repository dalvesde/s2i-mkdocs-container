# S2I MkDocs Container

> [Source-to-Image (S2I)](https://docs.openshift.com/container-platform/3.11/creating_images/s2i.html) is a framework that makes it easy to write images that take application source code as an input and produce a new image that runs the assembled application as output. The main advantage of using S2I for building reproducible container images is the ease of use for developers. As a builder image author, you must understand two basic concepts in order for your images to provide the best possible S2I performance: the build process and S2I scripts.

The S2I MkDocs Container should pull sources from a git repository, build them into an MkDocs static site and serve them. Serving a static site is a simple use case that can already be achieved with the [S2I Nginx Container](https://github.com/sclorg/nginx-container). Therefore to create the S2I MkDocs Container the best solution is to enchance the S2I Nginx Container and have it build the MkDocs static site before serving it.

That can be achieved in 2 steps:
1) Enhance the `Dockerfile` to install the MkDocs Python libraries;
2) Enhancing the `assemble` script (*the `assemble` script builds the application artifacts from a source and places them into appropriate directories inside the image*) to build the sources into an MkDocs static site.

The rest of the process is left unchanged, since the S2I Nginx Container already does everything we want.

In the `Dockerfile` we set up all the necessary labels to overwrite the ones coming from the S2I Nginx Container in order to correctly describe the S2I MkDocs Container.

## How to make the S2I MkDocs container available on OpenShift

First, the OpenShift administrators have to create the MkDocs Image Stream in the OpenShift namespace. Once this is created we can check it out by running `oc describe is/mkdocs -n openshift` with the OpenShift client tools.

In order to be able to actually edit the MkDocs Image Stream we need a token that grants us permission via a special account. The OpenShift administrators can generate this token. To use this token we can login as such `oc login https://openshift.cern.ch --token={TOKEN}`.

With the right permissions and the right Openshift CLI version (v3.10.0) we now have to connect the MkDocs Image Stream to the S2I MkDocs Container. This is achieved by tagging the image into the image stream: `oc tag --source=docker gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container:latest openshift/mkdocs:1.0 --reference-policy=local`. This will create the tag name (`1.0` in this case) if it doesn't already exist. Once the this is tagged we can check it out by running `oc describe istag/mkdocs:1.0 -n openshift`.

In order to have the S2I MkDocs Container appear in the web console application catalog we need to add the `builder` annotation to the image stream so that OpenShift knows it is an S2I builder. To edit to image stream and add annotations we need to run `oc edit is/mkdocs -n openshift`. The annotations should look something like this:
```yaml=
spec:
  tags:
  - annotations:
      description: ...
      iconClass: fa fa-book
      openshift.io/display-name: MkDocs
      sampleRepo: https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example.git
      tags: builder,MkDocs
```

## Resources

* To run the recommended version of the OpenShift client tools we can simply run `docker run -it gitlab-registry.cern.ch/paas-tools/openshift-client:v3.10.0 /bin/bash` and then execute our `oc` commands.
* [How to Create an S2I Builder Image](https://blog.openshift.com/create-s2i-builder-image/) blog post.
* [Jupyter on OpenShift Part 7: Adding the Image to the Catalog](https://blog.openshift.com/jupyter-on-openshift-part-7-adding-the-image-to-the-catalog/) blog post.
* OpenShift template annotations: `iconClass`: *An icon to be displayed with your template in the web console. Choose from our existing logo icons when possible. You can also use icons from FontAwesome and PatternFly. Alternatively, provide icons through CSS customizations that can be added to an OpenShift Container Platform cluster that uses your template. You must specify an icon class that exists, or it will prevent falling back to the generic icon.*. When using icons from FontAwesome, make sure to select icons from the [appropriate version](https://fontawesome.com/v4.7.0/icons/).

## Notes

* The [`site_url`](https://www.mkdocs.org/user-guide/configuration/#site_url) MkDocs configuration setting should end in a trailing slash (`/`) to avoid broken links in 404 pages. For example, <https://yourdocs.docs.cern.ch/inexistent_page/inexistent_page> will have broken links unless `site_url` is set to `https://yourdocs.docs.cern.ch/` (notice the trailing slash). See also:
    * https://github.com/mkdocs/mkdocs/issues/77#issuecomment-242777289
    * https://github.com/mkdocs/mkdocs/issues/1317